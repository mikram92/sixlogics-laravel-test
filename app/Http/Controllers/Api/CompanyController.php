<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\CompanyRequest;
use App\Http\Resources\CompanyCollection;
use App\Http\Resources\CompanyResource;
use App\Services\CompanyService;
use App\Traits\ApiHelper;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;

class CompanyController extends Controller
{
    use ApiHelper;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        $response = CompanyService::make()->index();
        return CompanyResource::collection($response);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(CompanyRequest $request)
    {
        try {
            $response = CompanyService::make()->create($request);
            return $this->success(200, $response, 'Company is Successfully Created!');
        } catch (QueryException $ex) {
            return $this->error(422, '', 'Something Went Wrong!');

        }
    }
    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function show($id)
    {
        $response = CompanyService::make()->show($id);
        return CompanyResource::collection($response);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(CompanyRequest $request, $id)
    {
        try {
            $response = CompanyService::make()->update($request, $id);
            return $this->success(200, $response, 'Company is Successfully Updated!');
        } catch (QueryException $ex) {
            return $this->error(422, '', 'Something Went Wrong!');

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $response = CompanyService::make()->delete($id);

        return $this->success(200, $response, 'Company is Successfully Deleted!');
    }
}
