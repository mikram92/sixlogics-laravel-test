<?php


namespace App\Services;


use App\Http\Requests\EmployeeCompanyUpdateRequest;
use App\Http\Requests\EmployeeRequest;
use App\Models\Employee;
use Carbon\Carbon;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\DB;

class EmployeeService extends Service
{

    /**
     * @return Employee[]|\Illuminate\Database\Eloquent\Collection
     */
    public function index()
    {
        $employeelist = Employee::all();
        return $employeelist;
    }

    /**
     * @param EmployeeRequest $request
     * @return bool
     */
    public function create(EmployeeRequest $request)
    {
        $employee = new Employee();
        $employee->name = $request['name'];
        $employee->position = $request['position'];
        $employee->company_id = $request['company_id'];
        return $employee->save();
    }

    /**
     * @param EmployeeCompanyUpdateRequest $request
     * @return mixed
     */
    public function updateEmployeeCompany(EmployeeCompanyUpdateRequest $request)
    {
        $companyUpdate = Employee::where('id', $request['employee_id'])
            ->update([
                'company_id' => $request['company_id'],
            ]);
        return $companyUpdate;

    }

    /**
     * @param $id
     * @return mixed
     */
    public function show($id)
    {
        $employeeinfo = Employee::where('id', $id)
            ->first();
        return $employeeinfo;
    }

    /**
     * @param EmployeeRequest $request
     * @param $id
     * @return mixed
     */
    public function update(EmployeeRequest $request, $id)
    {
        $employeeinfo = Employee::find($id);
        $employeeinfo->name = $request['name'];
        $employeeinfo->position = $request['position'];
        return $employeeinfo->save();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {
        $employee = Employee::findOrFail($id);
        return $employee->delete();
    }
}
