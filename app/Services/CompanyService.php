<?php


namespace App\Services;


use App\Http\Requests\CompanyRequest;
use App\Models\Company;
use App\Traits\ApiHelper;
use Carbon\Carbon;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\DB;

class CompanyService extends Service
{
    /**
     * @return Company[]|\Illuminate\Database\Eloquent\Collection
     */
    public function index()
    {
        return $companylist = Company::with('employees')->get();

    }

    /**
     * @param CompanyRequest $request
     * @return bool
     */
    public function create(CompanyRequest $request)
    {


        $company = new Company();
        $company->name = $request['name'];
        $company->address = $request['address'];
        return $company->save();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function show($id)
    {
        return Company::where('id', $id)->with('employees')
            ->get();

    }


    /**
     * @param CompanyRequest $request
     * @param $id
     * @return mixed
     */
    public function update(CompanyRequest $request, $id)
    {
        $company = Company::find($id);
        $company->name = $request['name'];;
        $company->address = $request['address'];

        return $company->save();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {
        $company = Company::findOrFail($id);
        return $company->delete();
    }
}
