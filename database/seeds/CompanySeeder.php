<?php

use App\Models\Company;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;

class CompanySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $faker = Faker::create();
        $company = new Company();
        foreach (range(1, 10) as $index) {
            $arr[] = ['name' => $faker->company,
                'address' => $faker->address,
            ];
            $arr++;

        }
        $company->insert($arr);
    }
}
