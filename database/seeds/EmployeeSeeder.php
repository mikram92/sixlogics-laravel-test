<?php

use App\Models\Company;
use App\Models\Employee;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\DB;

class EmployeeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        $employee = new Employee();
        foreach(Company::all() as $c){
            foreach (range(1,20) as $index) {
                $arr[] = [
                    'name' => $faker->name,
                    'company_id' => $c['id'],
                    'position' => $faker->jobTitle,
                ];
            }
        }
        $employee->insert($arr);
    }
}
