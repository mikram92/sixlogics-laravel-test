##SixLogic Laravel Test

<p>1. composer Install Command</p>

<p>2. npm install</p>

<p>3. Copy the example env file and make the required configuration changes in the .env file </p>

cp .env.example .env

<p> 4. Generate a new application key </p>

php artisan key:generate

<p> 5. Run the database migrations (Set the database connection in .env before migrating) </p>

php artisan migrate

<p> 6 Install passport: </p>

php artisan passport:install

<p> 7.Start the local development server </p>

php artisan serve

##Database seeding

Run the database seeder and you're done

php artisan db:seed

Note : It's recommended to have a clean database before seeding. You can refresh your migrations at any point to clean the database by running the following command

php artisan migrate:refresh

##POSTMAN COLLECTION URL

https://www.getpostman.com/collections/55f357504dc378ecb994
