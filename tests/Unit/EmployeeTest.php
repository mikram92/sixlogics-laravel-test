<?php

namespace Tests\Unit;


use App\Models\Company;
use App\Models\Employee;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class EmployeeTest extends TestCase
{
    use DatabaseMigrations;



    public function testGetEmployee()
    {
        $response = $this->json('GET', '/api/employees/');
        $response->assertStatus(200)
            ->assertJson([
                'data' => array()
            ]);

    }


    public function testCreateEmployee()
    {
        $company = factory(Company::class)->create();
        $company=  factory(Employee::class, 1)->create([
            'company_id' => Company::Find(1)->id]);
        $data = [
            'name' => "Test Employee",
            'position' => "test Position",
            'company_id' => $company[0]['company_id'],
        ];
        $response = $this->json('POST', '/api/employees', $data);
        $response->assertStatus(200)
            ->assertJson([
                'data' => true
            ]);

    }


    public function testUpdateEmployeeCompany()
    {
        $company = factory(Company::class)->create();
        $company=  factory(Employee::class, 1)->create([
            'company_id' => Company::Find(1)->id]);
        $data = [
            'company_id' => $company[0]['company_id'],
        ];
        $response = $this->json('POST', '/api/employees/1/company', $data);
        $response->assertStatus(200)
            ->assertJson([
                'data' => true
            ]);

    }
}
