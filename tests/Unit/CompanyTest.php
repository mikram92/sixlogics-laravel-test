<?php

namespace Tests\Unit;


use App\Models\Company;
use App\Models\Employee;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class CompanyTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testCreateCompany()
    {
        $data = [
            'name' => "Test Company",
            'address' => "Test Address",
        ];
        $response = $this->json('POST', '/api/companies', $data);
        $response->assertStatus(200)
            ->assertJson([
                'data' => true
            ]);

    }

    public function testGetCompanies()
    {
        $response = $this->json('GET', '/api/companies/',[]);
        $response->assertStatus(200)
            ->assertJson([
                'data' => array()
            ]);

    }

}
